/*
 * Init global variables
 */

Game newGame = new Game();

/*
 * Begining of the program
 */

void setup() {
    size(900, 900);
    
    newGame = new Game();
}

void draw() {
    newGame.drawGrid();

    if(newGame.isFinished()) {
        newGame.drawWinner();
    }

    noLoop();
}

void mousePressed() {
    if(!newGame.isFinished()) {
        newGame.nextTurn(mouseX, mouseY);
        redraw();
    }
}