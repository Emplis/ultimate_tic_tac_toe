public class Game {
    private static final int cross                  = -1;
    private static final int circle                 = 1;
    private static final int numberOfCellPerLine    = 9;
    private static final int littleDraw             = 999; // Value of draw for superGrid

    private color backgroundColor;

    private int turn;

    private int grid[][];
    private int superGrid[][];
    private int allowedPlacement[][];



    public Game() {
        this.turn       = 0;
        this.grid       = new int[9][9];
        
        this.superGrid = new int[][] {
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0}
        };
        
        this.allowedPlacement = new int[][] {
            {1, 1, 1},
            {1, 1, 1},
            {1, 1, 1}
        };

        this.initGrid();
        
        this.backgroundColor = color(170, 170, 170);
    }



    private void initGrid() {
        for(int i = 0; i < this.numberOfCellPerLine; i++) {
            for(int j = 0; j < this.numberOfCellPerLine; j++) {
                this.grid[i][j] = 0;
            }
        }
    }



    /*
     * -----------------------------------------------------------------------------------------------------------------
     * x and y are the position of the mouse
     */

    public void nextTurn(int x, int y) {
        // -------------------------------------------------------------------------------------------------------------
        // Variables declaration

        int currentPlayer; // Used to know which player is actually playing

        // Cell where the player clicked on the 9x9 grid

        int cellPositionX;
        int cellPositionY;

        // SuperCell where the player clicked on the 3x3 grid

        int cellPositionSuperX;
        int cellPositionSuperY;

        // Variable used for the winning detection algorithm

        int firstDiagonal;
        int secondDiagonal;
        int column;
        int line;
        int relativeCellPositionX;
        int relativeCellPositionY;
        int isADraw;

        // -------------------------------------------------------------------------------------------------------------
        // Begining of the function

        currentPlayer = (this.turn % 2 == 0) ? this.circle : this.cross;

        cellPositionX = this.coordinatesToCellPosition(x);
        cellPositionY = this.coordinatesToCellPosition(y);

        cellPositionSuperX = 2;
        cellPositionSuperY = 2;

        if(cellPositionX < 3) {
            cellPositionSuperX = 0;
        }
        else if(cellPositionX < 6) {
            cellPositionSuperX = 1;
        }

        if(cellPositionY < 3) {
            cellPositionSuperY = 0;
        }
        else if(cellPositionY < 6) {
            cellPositionSuperY = 1;
        }

        // We test if the "superCell" is free (== 0) and if we can play on this "superCell"

        if(this.superGrid[cellPositionSuperY][cellPositionSuperX] == 0 
        && this.allowedPlacement[cellPositionSuperY][cellPositionSuperX] == 1) {

            // We test if the cell has not been marked 

            if(this.grid[cellPositionY][cellPositionX] == 0) {
                
                // Application of the mark on the clicked cell 
                
                this.grid[cellPositionY][cellPositionX] = currentPlayer;

                // Update the status of the "superGrid" by testing if the previous move allow the current player
                // to win a "superCell" or create a draw "superCell"

                firstDiagonal   = 0;
                secondDiagonal  = 0;
                column          = 0;
                line            = 0;

                relativeCellPositionX = (cellPositionX - (cellPositionX % 3));
                relativeCellPositionY = (cellPositionY - (cellPositionY % 3));

                for(int i = 0; i < 3; i++) {
                    column  = 0;
                    line    = 0;

                    if(this.grid[relativeCellPositionY + i][relativeCellPositionX + i] == currentPlayer) {
                        secondDiagonal++;
                    }

                    if(this.grid[relativeCellPositionY + i][(relativeCellPositionX + 2) - i] 
                    == currentPlayer) {
                        firstDiagonal++;
                    }

                    for(int j = 0; j < 3; j++) {
                        if(this.grid[relativeCellPositionY + i][relativeCellPositionX + j] == currentPlayer) {
                            column++;
                        }

                        if(this.grid[relativeCellPositionY + j][relativeCellPositionX + i] == currentPlayer) {
                            line++;
                        }
                    }

                    if(column == 3 || line == 3) {
                        this.superGrid[cellPositionSuperY][cellPositionSuperX] = currentPlayer;
                    }
                }

                if(firstDiagonal == 3 || secondDiagonal == 3) {
                    this.superGrid[cellPositionSuperY][cellPositionSuperX] = currentPlayer;
                }

                if(this.superGrid[cellPositionSuperY][cellPositionSuperX] != currentPlayer) {
                    isADraw = 0;

                    for(int i = 0; i < 3; i++) {
                        for(int j = 0; j < 3; j++) {
                            isADraw += (this.grid[relativeCellPositionY + i][relativeCellPositionX + j] == 0) ? 0 : 1;
                        }
                    }

                    if(isADraw == 9) {
                        this.superGrid[cellPositionSuperY][cellPositionSuperX] = this.littleDraw;
                    }
                }

                // Test to know on which "superCell" we will be allowed to play the next turn

                if(this.superGrid[cellPositionY % 3][cellPositionX % 3] == this.cross
                || this.superGrid[cellPositionY % 3][cellPositionX % 3] == this.circle
                || this.superGrid[cellPositionY % 3][cellPositionX % 3] == this.littleDraw) {

                    this.allowedPlacement = new int[][] {
                        {1, 1, 1},
                        {1, 1, 1},
                        {1, 1, 1}
                    };
                }
                else {
                    this.allowedPlacement = new int[][] {
                        {0, 0, 0},
                        {0, 0, 0},
                        {0, 0, 0}
                    };

                    allowedPlacement[cellPositionY % 3][cellPositionX % 3] = 1;
                }

                // Let's go for another turn

                this.turn++;
            }
        }
    }



    public int coordinatesToCellPosition(int coordinate) {
        // width always equal height
        return (int)(coordinate * this.numberOfCellPerLine / width);
    }



    /*
     * -----------------------------------------------------------------------------------------------------------------
     * Draw symbol
     * symbol can be strictly equal to circle and cross vars
     * if method == true, then draw "super" symbol
     * else, draw "normal" symbol
     */

    public void drawSymbol(int line, int column, int symbol, boolean method) {
        int x;
        int y;

        int x0;
        int y0;
        int x1;
        int y1;

        int radiusSize;

        int strokeColor;
        color fillColor;

        radiusSize  = (int)((width / this.numberOfCellPerLine) * 0.75);
        strokeColor = 187;

        if(method) {
            // A faire affichage quand on remporte un 3x3
            noStroke();
            rectMode(CENTER);

            x           = (int)(column * (width / 3) + (width / 3) / 2);
            y           = (int)(line * (height / 3) + (height / 3) / 2);
            radiusSize  = (int)(width / 3);

            if(symbol == this.circle) {
                fillColor = color(strokeColor, 0, 0, 50);
            }
            else if(symbol == this.cross) {
                fillColor = color(0, 0, strokeColor, 50);
            }
            else {
                fillColor = color(25, 25, 25, 50);
            }

            fill(fillColor);
            rect(x, y, radiusSize, radiusSize);
        }
        else {
            x = (int)(column * (width / this.numberOfCellPerLine) + (width / this.numberOfCellPerLine) / 2);
            y = (int)(line * (height / this.numberOfCellPerLine) + (height / this.numberOfCellPerLine) / 2);

            strokeWeight(4);

            if(symbol == this.circle) {
                stroke(color(strokeColor, 0, 0));

                ellipseMode(CENTER);
                fill(this.backgroundColor);
                ellipse(x, y, radiusSize, radiusSize);
            }
            else if(symbol == this.cross) {
                stroke(color(0, 0, strokeColor));

                x0 = (int)(x - (radiusSize / 2));
                y0 = (int)(y - (radiusSize / 2));
                
                x1 = (int)(x + (radiusSize / 2));
                y1 = (int)(y + (radiusSize / 2));

                line(x0, y0, x1, y1);

                x0 = (int)(x - (radiusSize / 2));
                y0 = (int)(y + (radiusSize / 2));
                
                x1 = (int)(x + (radiusSize / 2));
                y1 = (int)(y - (radiusSize / 2));

                line(x0, y0, x1, y1);
            }
        }
    }



    void drawGrid() {
        //--------------------------------------------------------------------------------------------------------------

        background(this.backgroundColor);

        //--------------------------------------------------------------------------------------------------------------

        int x0;
        int y0;
        int x1;
        int y1;

        stroke(1);

        for(int i = 0; i < this.numberOfCellPerLine; i++) {
            strokeWeight(1);

            if(i == 3 || i == 6) strokeWeight(4);

            // Coordinates for columns

            x0 = i * width / this.numberOfCellPerLine;
            y0 = 0;
            x1 = i * width / this.numberOfCellPerLine;
            y1 = height;

            line(x0, y0, x1, y1);

            // Coordinates for lines

            x0 = 0;
            y0 = i * height / this.numberOfCellPerLine;
            x1 = width;
            y1 = i * height / this.numberOfCellPerLine;

            line(x0, y0, x1, y1);
        }

        //--------------------------------------------------------------------------------------------------------------

        for(int i = 0; i < numberOfCellPerLine; i++) {
            for(int j = 0; j < numberOfCellPerLine; j++) {
                if(this.grid[i][j] != 0) {
                    drawSymbol(i, j, this.grid[i][j], false);
                    //println("Hi!");
                }
            }
        }

        //--------------------------------------------------------------------------------------------------------------

        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                if(this.superGrid[i][j] != 0) {
                    drawSymbol(i, j, this.superGrid[i][j], true);
                    //println("Hi!");
                }
            }
        }

        //--------------------------------------------------------------------------------------------------------------
    }



    void drawWinner() {
        int winner;
        int x;
        int y;
        color textColor;
        String text;

        winner = this.getWinner();
        x = (int)(width / 2);
        y = (int)(height / 2);

        textAlign(CENTER, BOTTOM);
        textSize(80);

        if(winner != 0) {
            //println(winner);
            if(winner == this.circle) {
                textColor = color(255, 0, 0);
                text = "Circle win!";
            }
            else {
                textColor = color(0, 0, 255);
                text = "Cross win!";
            }
        }
        else {
            textColor = color(25, 25, 25);
            text = "It's a draw!";
        }

        fill(textColor);
        text(text, x, y);
    }



    /*
     * Return circle if circle win, cross if cross win and 0 if there is no winner
     */

    public int getWinner() {
        int firstDiagonal;
        int secondDiagonal;
        int column;
        int line;
        
        int isADraw;

        int playerLastTurn;

        playerLastTurn  = (this.turn % 2 == 0) ? this.cross : this.circle;
        firstDiagonal   = 0;
        secondDiagonal  = 0;

        for(int i = 0; i < 3; i++) {
            
            column  = 0;
            line    = 0;

            if(this.superGrid[i][i] == playerLastTurn) {
                secondDiagonal++;
            }

            if(this.superGrid[i][2 - i] == playerLastTurn) {
                firstDiagonal++;
            }

            for(int j = 0; j < 3; j++) {
                if(this.superGrid[i][j] == playerLastTurn) {
                    column++;
                }

                if(this.superGrid[j][i] == playerLastTurn) {
                    line++;
                }
            }

            if(column == 3 || line == 3) {
                return playerLastTurn;
            }
        }

        if(firstDiagonal == 3 || secondDiagonal == 3) {
            return playerLastTurn;
        }

        // Here no one win "normaly"

        return this.getWinnerByNumberOfCells();
    }

    public int getWinnerByNumberOfCells() {
        int numberOfCircleCells;
        int numberOfCrossCells;
        int numberOfDrawCells;

        numberOfCircleCells = 0;
        numberOfCrossCells  = 0;
        numberOfDrawCells   = 0;

        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                if(this.superGrid[i][j] == this.circle) {
                    numberOfCircleCells++;
                }
                else if(this.superGrid[i][j] == this.cross) {
                    numberOfCrossCells++;
                }
                else if(this.superGrid[i][j] == this.littleDraw) {
                    numberOfDrawCells++;
                }
            }
        }

        if((numberOfCircleCells + numberOfCrossCells + numberOfDrawCells) == 9) {
            if(numberOfCircleCells > numberOfCrossCells) {
                return this.circle;
            }
            else if(numberOfCircleCells < numberOfCrossCells) {
                return this.cross;
            }
        }

        return 0;
    }

    public boolean isDraw() {
        int isADraw;

        isADraw = 0;

        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                // If all the cell are different of 0 then no more cell can be win
                isADraw += (this.superGrid[i][j] != 0) ? 1 : 0; 
            }
        }

        return (this.getWinner() == 0 && isADraw == 9);
    }

    public boolean isFinished() {
        return (this.getWinner() != 0) || this.isDraw();
    }
}